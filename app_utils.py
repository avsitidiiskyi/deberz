import time
from functools import wraps


def retry(max_retries=3, wait_time=1):
    def decorator_retry(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            for _ in range(max_retries + 1):
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    print(f"Error: {e}. Retrying...")
                    time.sleep(wait_time)
            print(f"Maximum number of retries ({max_retries}) reached. Aborting.")
            return None

        return wrapper

    return decorator_retry
