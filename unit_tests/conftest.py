import pytest
from core_file import shuffle_deck


@pytest.fixture
def shuffled_deck():
    deck = shuffle_deck()
    yield deck