from core_file import shuffle_deck, deal_cards, add_three_cards_to


def test_shuffle_deck():
    deck_one = shuffle_deck()
    deck_two = shuffle_deck()
    assert deck_one != deck_two


def test_32_cards_in_deck(shuffled_deck):
    assert len(shuffled_deck) == 32


def test_cards_in_deck_after_dealing_two_players(shuffled_deck):
    _, __ = deal_cards(shuffled_deck)
    assert (len(shuffled_deck)) == 20


def test_cards_in_deck_after_dealing_three_additional_cards_to_user(shuffled_deck):
    user_one, _ = deal_cards(shuffled_deck)
    add_three_cards_to(user=user_one, deck=shuffled_deck)
    assert (len(shuffled_deck)) == 17

