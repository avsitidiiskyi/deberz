from core_file import *


def test_deal_cards(shuffled_deck):

    user_one, user_two = deal_cards(shuffled_deck)
    assert len(user_one) == 7
    assert len(user_two) == 7
    assert isinstance(user_one[0], list)
    assert isinstance(user_two[0], list)


def test_array_of_dealt_cards_initially_is_empty(shuffled_deck):
    user_one, user_two = deal_cards(shuffled_deck)
    assert len(user_one[0]) == 0


def test_three_additional_cards_are_dealt(shuffled_deck):
    empty_array = []
    add_three_cards_to(empty_array, shuffled_deck)
    assert len(empty_array) == 3


def test_three_cards_are_added_to_user_after_six_are_dealt(shuffled_deck):
    neo, morpheus = deal_cards(shuffled_deck)
    add_three_cards_to(neo, shuffled_deck)
    assert len(neo) == 10
