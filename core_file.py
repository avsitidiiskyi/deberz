import random

from app_utils import retry

suites: list = ['diamond', 'hearts', 'clubs', 'spades']
cards: list = ['seven', 'eight', 'nine', 'ten', 'jack', 'queen', 'king', 'ace']


def shuffle_deck() -> list:
    deck: list = []
    for suite in suites:
        for card in cards:
            deck.append(f'{suite}.{card}')

    [random.shuffle(deck) for _ in range(3)]
    return deck


card_values = {
    "hearts.seven": 0,
    'hearts.eight': 0,
    "hearts.nine": 0,
    "hearts.ten": 10,
    "hearts.jack": 2,
    "hearts.queen": 3,
    "hearts.king": 4,
    "hearts.ace": 11,

    "diamond.seven": 0,
    'diamond.eight': 0,
    "diamond.nine": 0,
    "diamond.ten": 10,
    "diamond.jack": 2,
    "diamond.queen": 3,
    "diamond.king": 4,
    "diamond.ace": 11,

    "spades.seven": 0,
    'spades.eight': 0,
    "spades.nine": 0,
    "spades.ten": 10,
    "spades.jack": 2,
    "spades.queen": 3,
    "spades.king": 4,
    "spades.ace": 11,

    "clubs.seven": 0,
    'clubs.eight': 0,
    "clubs.nine": 0,
    "clubs.ten": 10,
    "clubs.jack": 2,
    "clubs.queen": 3,
    "clubs.king": 4,
    "clubs.ace": 11
}


def _deal_three_cards_to(player: list, deck: list) -> None:
    for dealing in range(3):
        card = deck[0]
        player.append(card)
        deck.remove(card)


def deal_cards(deck: list) -> tuple[list, list]:
    """
    Function for dealing cards to each user, three at a time, 6 total. Currently implemented for two players only

    :return: two lists of users with dealt cards from the deck
    """

    player_one: list = [[]]
    player_two: list = [[]]

    for _ in deck:
        for dealing in range(2):
            _deal_three_cards_to(player_one, deck)
            _deal_three_cards_to(player_two, deck)
        break

    return player_one, player_two


def add_three_cards_to(user: list, deck: list):
    _deal_three_cards_to(user, deck)


@retry(max_retries=5, wait_time=0.1)
def sort_trumps_for(user: list) -> None:
    for card in user:
        if isinstance(card, str) and card.split(".")[0] == trump_suit:
            user.remove(card)
            user[0].append(card)
    for card in user[1:]:
        assert trump_suit not in card


def points(user: list) -> list:
    def _trump_points(player: list) -> list:
        trump_points = []
        for card in player[0]:
            for k in card_values.keys():
                if card == k and 'jack' in card:
                    trump_points.append(20)
                elif card == k and 'nine' in card:
                    trump_points.append(14)
                else:
                    if card == k:
                        trump_points.append(card_values[k])
        return trump_points

    def _non_trump_points(player: list) -> list:
        non_trump_points = []
        for card in player:
            for k in card_values.keys():
                if card == k:
                    non_trump_points.append(card_values[k])
        return non_trump_points

    return _trump_points(user) + _non_trump_points(user)


if __name__ == '__main__':  # todo: combinations, bribes
    deck: list = shuffle_deck()

    john_wick, james_bond = deal_cards(deck)

    trump = deck[0]
    deck.remove(trump)
    trump_suit = trump.split(".")[0]
    freeze = deck[-1]

    print(f"{trump=}")
    print(f"{trump_suit=}")
    print(f"{freeze=}")

    print()
    add_three_cards_to(john_wick, deck)
    add_three_cards_to(james_bond, deck)
    sort_trumps_for(john_wick)
    sort_trumps_for(james_bond)
    print()
    print(f"{john_wick=}")
    print(points(john_wick))

    print(f"{james_bond=}")
    print(points(james_bond))

    print(f"CARDS IN DECK REMAINING = {deck} ")
